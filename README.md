# TREXI - linear

This repository contains the analysis plan for the first, linear, analyses of the TREXI-Study (Treatment Expectation and their Influence on Infiltration outcome). It aims to explain and predict, respectively, the treatment outcome of lumbar infiltrations to treat back pain based on questionnaire data. The analyses are strongly inspired by Bayesian theories of perception and homeostatic control.

People involved in the project (sorted alphabetically):

- Barth, Jürgen
- Borges Pereira, Inês
- Heinzle, Jakob
- Manjaly, Zina M.
- Müller-Schrader, Matthias S. J.
- Petzschner, Frederike H.
- Stephan, Klaas E.
- Toussaint, Birte
- Wiech, Katja
- Witt, Claudia

Author: Matthias Müller-Schrader


## Revisions

| 30.07.2020 |	Original version 	|							 |
| 05.08.2020 |  Revision 1			| Removal of ICI measures.   |
| 28.10.2020 |  Revision 2			| Added additional analysis with EST.   |
 